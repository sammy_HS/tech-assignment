import { UPDATE_FIELD } from '../actions/input_actions';

function form(state = {}, action) {
    switch(action.type) {
        case UPDATE_FIELD:
            return {
                ...state,
                [action.payload.field]: action.payload.value,
            };
        default:
            return state;
    }
}

export default form;
