import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import form from './reducers/input_reducers';

const store = createStore(form, applyMiddleware(logger));

export default store;
