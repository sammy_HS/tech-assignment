export const UPDATE_FIELD = 'UPDATE_FIELD';

export function updateField(field, value) {
    return {
        type: UPDATE_FIELD,
        payload : {
            field,
            value,
        }
    };
}
