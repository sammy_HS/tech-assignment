export const InputConfig = {
    'title' : {
        required: false,
        type: 'text',
        placeholder: 'Briefly mention your problem.',
        classes: "mb-20",
        name: 'title',
        label: 'Issue Title',
    },
    'author-name' : {
        required: false,
        type: 'text',
        placeholder: 'Enter your full name',
        classes: "mb-20",
        name: 'author-name',
        label: 'Name',
    },
    'email' : {
        required: true,
        type: 'text',
        placeholder: 'Enter your email',
        classes: "mb-20",
        name: 'email',
        label: 'Email',
        validation: true,
    },
    'message-body' : {
        required: true,
        type: 'textarea',
        placeholder: 'What problems are you facing?',
        classes: "mb-20",
        name: 'message-body',
        label: 'What issue are you having?',
    },
    'tags' : {
        required: false,
        type: 'text',
        placeholder: 'Enter some tags',
        name: 'tags',
        diffOnChange : true, // the others will be using one function & using this field we can use another function.
        label: 'Tags',
        classes: ''
    }
};

export const text_fields = Object.keys(InputConfig);
