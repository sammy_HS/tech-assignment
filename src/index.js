import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './root-store/store.index';
import App from './components/App';

import './styles/index.scss';

const Index = () => (
    <React.Fragment>
        <Provider store={store}>
            <App />
        </Provider>
    </React.Fragment>
);

ReactDOM.render(<Index />, document.getElementById('root'));
