class Notifier {
    support = false

    checkSupportAndPermission() {
        if (window.Notification) {
            this.support = true;
        } else {
            alert("Notifications aren't supported! Sorry!");
        }
    }

    checkSupport () {
        return this.support;
    }

    checkPermission () {
        return Notification.permission;
    }

    seekPermission() {
        if (this.checkSupport()) {
            if (Notification.permission !== 'denied') {
                Notification.requestPermission(status => {
                    this.support = true;
                    new Notification('Welcome!');
                });
            }
        }
    }

    showSuccessNotification(body) {
        if (this.checkSupport()) {
            const notif = new Notification('Success!', {
                body,
            });
    
            setTimeout(notif.close(), 3500);
        }
    }

    showFailureNotification(body) {
        if (this.checkSupport()) {
            const notif = new Notification('Failure!', {
                body,
            });
    
            setTimeout(notif.close(), 3500);
        }
    }
}

const NotificationManager = new Notifier();

export default NotificationManager;
