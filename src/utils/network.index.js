const BASE_URL = 'https://api.helpshift.com/v1/infinitelybeta/issues';

export async function makeIssue(body) {
    const request = await fetch(BASE_URL, {
        method: 'POST',
        body,
        headers: {
            'Accept' : 'application/json',
            'Authorization' : 'Basic aW5maW5pdGVseWJldGFfYXBpXzIwMTkwMTEwMTExMjA0MDE5LTIzOTM3YjVlZGQ3MmExYw==',
        },
    });

    return request;
}