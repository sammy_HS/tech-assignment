export const checkValidEmail = (email) => {
    var email_re = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return email_re.test(String(email).toLowerCase());
}

export const getSeconds = (time_low, time_high) => {
    const dateDiff = time_high - time_low;
    return (dateDiff / 1000);
}
