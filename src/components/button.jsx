import React from 'react';
import '../styles/button.scss';

class Button extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isClicked: false,
        };
    }

    onClickButton = e => {
        e.preventDefault();
        this.props.onClick();
    }

    render () {
        const { style, label, isDisabled } = this.props;
        return (
            <button
                className="action-button"
                style={style}
                onClick={this.onClickButton}
                disabled={isDisabled}
            >
                {/* <div className="inside-button">
                    {
                        isDisabled ? (
                            <div className="loading-spinner" />
                        ) : null
                    }
                </div> */}
                {label}
            </button>
        );
    }
}

export default Button;
