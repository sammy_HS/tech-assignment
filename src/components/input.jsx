import React from "react";

import "../styles/input.scss";

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.isValidityUsed = false;
  }

  validityChecker(value) {
    if (value.length === 0) {
      return null;
    }

    if (value === undefined) {
      return null;
    }

    if (value.length > 0) {
      if (this.props.validityChecker(value)) {
        return (
          <span className="valid-label" role="img" aria-label="Correct Email">
            ✅
          </span>
        );
      } else {
        return (
          <span className="valid-label" role="img" aria-label="Incorrent Email">
            ❌
          </span>
        );
      }
    }
  }

  render() {
    const {
        type,
        label,
        name,
        required,
        style,
        className,
        rows,
        placeholder,
        value,
        onChangeText,
        validityChecker,
        textType,
    } = this.props;

    if (validityChecker !== undefined) {
      this.isValidityUsed = true;
    }

    return (
      <React.Fragment>
        <div className={`input-container ${className !== undefined ? className : ''}`} style={style}>
            <div className="label-container">
                <div className="label-wrapper">
                  <span className="input-label">{label}</span>
                  {
                    this.isValidityUsed ? this.validityChecker(this.props.value) : null
                  }
                </div>
                {
                    required ? <span className="required-text">Required</span> : null
                }
            </div>
            {
                type !== "textarea" ?
                <input
                    name={name}
                    className="text-input"
                    type={textType || "text"}
                    placeholder={placeholder}
                    value={value}
                    onChange={e => onChangeText(e)}
                /> :
                <textarea
                    name={name}
                    className="text-input"
                    rows={rows}
                    value={value}
                    placeholder={placeholder}
                    onChange={e => onChangeText(e)}
                />
            }
        </div>
      </React.Fragment>
    );
  }
}

export default Input;
