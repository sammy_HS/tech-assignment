import React from 'react';
import Content from './Content';
import Form from './Form';

import '../styles/app.scss';

const App = () => {
  return (
    <div className="app">
      <div className="page-content">
        <div className="content-panel">
          <Content />
        </div>
        <div className="form-panel">
          <Form />
        </div>
      </div>
    </div>
  );
};

export default App;
