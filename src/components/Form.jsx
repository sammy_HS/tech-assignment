import React from 'react';
import { connect } from 'react-redux';

import Button from './button';
import Input from './input';
import { checkValidEmail, getSeconds } from '../utils/utils.index';
import { updateField } from '../root-store/actions/input_actions';
import NotificationManager from '../utils/notifications.index';
import { makeIssue } from '../utils/network.index';
import { InputConfig, text_fields } from '../config/input.config';

import '../styles/form.scss';

class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            endTime: undefined,
            startTime: Date.now(),
            isButtonActive: true,
        };

        this.allIsSet = true;

        this.custom_fields = {
            'total_time' : {
                type: 'number',
                value: undefined,
            }
        };
        this.diffOnChange = {
            'tags' : this.onChangeTags
        };
        this.validations = {
            'email' : checkValidEmail
        };
    }

    componentDidMount() {
        NotificationManager.checkSupportAndPermission();
        if (NotificationManager.checkSupport()) {
            if (NotificationManager.checkPermission() !== 'granted') {
                NotificationManager.seekPermission();
            }
        }
    }

    onChangeText = (e) => {
        this.props.dispatch(updateField(e.target.name, e.target.value));
    }

    onChangeTags = (e) => {
        this.props.dispatch(updateField(e.target.name, e.target.value.split(",").map(str => str.trim())));
    }

    checkForRequired(field) {
        return this.props[field].length > 0;
    }

    isValidated(field) {
        let valid = true;
        if (InputConfig[field].validation) {
            valid = this.validations[field](this.props[field]);
        }
        return valid;
    }

    setupFormFields() {
        let formData = new FormData();

        text_fields.forEach(field => {
            if (this.props[field]) {
                console.log(field)
                if (InputConfig[field].required === true) {
                    if (this.checkForRequired(field) && this.isValidated(field)) {
                        formData.append(field, this.props[field]);
                    } else {
                        this.allIsSet = false;
                    }
                } else {
                    if (typeof this.props[field] === 'object') {
                        formData.append(field, JSON.stringify(this.props[field]));
                    } else {
                        formData.append(field, this.props[field]);
                    }
                }
            }
        });

        return formData;
    }

    setCustomValue(field, value) {
        this.custom_fields[field].value = value;
    }

    setupCustomFields(formData) {
        formData.append('custom_fields', JSON.stringify(this.custom_fields));
        return formData;
    }

    endTimer(cb) {
        let timeNow = Date.now();
        this.setState({
            endTime: timeNow,
            isButtonActive: false,
        }, () => {
            cb();
        });
    }

    resetButtonState() {
        let startTime = Date.now();
        this.setState({
            endTime: undefined,
            startTime,
            isButtonActive: true,
        });
    }

    checkFormData(formData) {
        let count = 0;
        for (let field of formData.keys()) {
            count += 1;
            console.log(field);
        }

        if (count >= 3) {
            return true;
        }

        return false;
    }

    sendForm = () => {
        let total_time = getSeconds(this.state.startTime, this.state.endTime);
        this.setCustomValue('total_time', total_time);

        let formData = this.setupFormFields();
        formData = this.setupCustomFields(formData);

        if (this.allIsSet && this.checkFormData(formData)) {
            makeIssue(formData)
                .then(res => {
                    if (res.ok) {
                        NotificationManager.showSuccessNotification('The Issue was successfully created!');
                        this.clearFields();
                    } else {
                        NotificationManager.showFailureNotification('There was a problem with the request!');
                    }
                })
                .catch(err => {
                    console.error(err);
                    NotificationManager.showFailureNotification('There was a problem while submitting the request');
                }).finally(() => {
                    this.resetButtonState();
                });
        } else {
            this.resetButtonState();
            NotificationManager.showFailureNotification('Some required fields are not filled correctly!');
        }
    }

    submit = () => {
        this.endTimer(this.sendForm);
    }

    clearFields = () => {
        text_fields.forEach(field => {
            this.props.dispatch(updateField(field, ''));
        });
    }

    render () {
        return (
            <React.Fragment>
                <div className="form-container">
                    <div className="form-inputs">
                        <div className="form-header">
                            <span className="main-text">Can't find what you're looking for?</span>
                            <span className="sub-text">Describe your issue below &amp; we'll get back to you soon.</span>
                        </div>
                        <div className="divider-form" />
                        {
                            text_fields.map(field => (
                                <Input
                                    key={InputConfig[field].name}
                                    type={InputConfig[field].type}
                                    label={InputConfig[field].label}
                                    placeholder={InputConfig[field].placeholder}
                                    className={InputConfig[field].classes}
                                    name={InputConfig[field].name}
                                    onChangeText={
                                        InputConfig[field]['diffOnChange'] === true ?
                                            this.diffOnChange[InputConfig[field].name] :
                                            this.onChangeText
                                    }
                                    value={this.props[field] || ""}
                                    required={InputConfig[field].required}
                                    validityChecker={InputConfig[field].validation ? this.validations[field] : undefined}
                                />
                            ))
                        }
                    </div>
                    <div className="form-action">
                        <Button
                            label="Submit Issue"
                            onClick={this.submit}
                            isDisabled={!this.state.isButtonActive}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        title: state.title,
        email: state.email,
        'author-name': state['author-name'],
        'message-body' : state['message-body'],
        tags: state.tags
    };    
}

export default connect(mapStateToProps)(Form);
