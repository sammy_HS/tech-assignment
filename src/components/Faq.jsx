import React from 'react';

import '../styles/faq.scss';

const faq = ["How do I find my size?", "How much is shipping?", "What is your return policy?", "When will my order arrive?", "How does International Standard Delivery work?"];

const FAQ = () => {
    return (
        <React.Fragment>
            <div className="faq-container">
            {
                faq.map(question => (
                    // eslint-disable-next-line jsx-a11y/anchor-is-valid
                    <a href="#" key={question} className="faq-item">
                        {question}
                    </a>
                ))
            }
            </div>
        </React.Fragment>
    );
}

export default FAQ;
