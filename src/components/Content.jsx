import React from 'react';
import FAQ from './Faq';
import searchIcon from '../images/icon-search.png';

import '../styles/content.scss';
import Input from './input';

const Content = () => {
    return (
        <React.Fragment>
            <div className="content-container">
                <div className="header">
                    Welcome to Shoppr Support
                </div>
                <div className="header-sub-text mb-40">
                    How can we help you today?
                </div>
                <div className="divider mb-40" />
                <div className="search-box mb-40">
                    <img src={searchIcon} alt="search-icon" height={24} width={24} />
                    <Input
                        placeholder="Search for answers"
                        onChangeText={(e) => {}}
                    />
                </div>
                <div className="faq-header mb-12">
                    Common Questions
                </div>
                <FAQ />
                <div className="view-more">
                    View all questions
                    &rarr;
                </div>
            </div>
        </React.Fragment>
    );
};

export default Content;
